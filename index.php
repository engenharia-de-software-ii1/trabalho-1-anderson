<?php
    require_once("funcoes.php");
?>
<!DOCTYPE html>
<html>
    
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <title>JavaScrpt API tutorial</title>
    <style>
        body {
            background-color: '#e6f2ff';
        }
        h1 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 14pt;
            padding: 20px 0 0px 0px;
        }
        input {
            margin-top: 10px;
            margin-left: 20px;
        }

        figure {
            margin-top: 20px;
            margin-left: 270px;
        }
    </style>
</head>

<body>
    <figure>
        <img src="calculadora.png" height="50">
    </figure>
    <form action="index.php" method="post">
        <div class="form-row">
            <div class="form-group col-md-6">
                <input type="text" class="form-control" id="n1" name="n1" placeholder="Primeiro numero">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <input type="text" class="form-control" id="n2" name="n2" placeholder="Segundo numero">
            </div>
        </div>
        <div class="col-auto">
            <button type="submit" name="calcular" id="calcular" class="btn btn-primary mb-2">Calcular</button>
            <?php


                if(isset($_POST["calcular"])) {
                    $a = $_POST["n1"];
                    $b = $_POST["n2"];
                    $s = Soma($a,$b);
                    $p = Produto($a, $b);
                    echo "<h1>Soma: &nbsp;" .$a. " + " .$b. " = " .$s. "</h1>";
                    echo "<h1>Produto: &nbsp;" .$a. " x " .$b. " = " .$p. "</h1>";
                }
                
            ?>
        </div>
    </form>
    <div id="eu">
    </div>
    <script>
        var output = document.getElementById("eu");
        function myFunction(){
            output.innerHTML = this.responseText;
        }

        var myRequest = new XMLHttpRequest();
        myRequest.onreadystatechange = myFunction;
        myRequest.open("GET","funcoes.php");
        myRequest.send();
        console.log(myRequest);
    </script>
</body>
</html>
