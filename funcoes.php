<?php
    function Soma($a, $b) {
        $c = $a + $b;
        if (VeriSoma($a,$b,$c) == true){
            return $c;
        }
        else
        {
            return 0;
        }
    }

    function Produto($a, $b) {
        $c = $a * $b;
        if (VeriProduto($a,$b,$c) == true){
            return $c;
        }
        else {
            return 0;
        }
    }

    function VeriSoma($x, $y, $z) {
        $a1 = ($x + $y) + $z == $x + ($y + $z);
        $a2 = $x + $y == $y + $x;
        $a3 = ($x + 0) == $x;
        $a4 = $x + (- $x) == 0;
        if ($a1 && $a2 && $a3 && $a4 == true) {
            return true;
        }
        else {
            return false;
        }
    }

    function VeriProduto($x, $y, $z) {
        $m1 = ($x * $y) * $z == $x * ($y * $z);
        $m2 = $x * $y == $y * $x;
        $m3 = $x * 1 == $x;
        $m4 = $x * pow($x,-1) == 1;
        if ($m1 && $m2 && $m3 && $m4 == true){
            return true;
        }
        else {
            return false;
        }
    }

?>
